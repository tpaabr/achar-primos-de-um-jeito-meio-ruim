#Chega ate 4X o limite
#Para obter um primo especifico dentro do limite = (indice + 1)
#*********************TENTAR METODOS PROGRAMAÇÃO DINAMICA
import time
start_time = time.time()


def primos(limite):
         
    resultados = [2] #Lista inicial
    primos = [2]
    
    #Variaveis aux
    a = 1
    i = 0
    y = 0
    k1 = 0
    k2 = 0 
    
    print("O primo de numero 1 é 2")
    while i < limite:
        
        n1 = 2 * resultados[i] - 1
        n2 = 2 * resultados [i] + 1
        
        #Retirar numeros duplicados
        if n2 in resultados and n1 not in resultados :
            resultados.append(n1)
        elif n1 in resultados and n2 not in resultados :
            resultados.append(n2)
        elif n1 not in resultados and n2 not in resultados :
            resultados.append(n1)
            resultados.append(n2)
            
        #Ver se é primo
        #Testando tamanho
        tamanho = len(primos)/3
        while y < tamanho :
            
            if n1 % primos[y] == 0 :
                k1 += 1
            elif n2 % primos[y] == 0:
                k2 += 1
            
            y += 1
        #Adicionar a lista
        if k1 == 0 :
            primos.append(n1)
            a += 1
            print("O primo de numero " +  str(a) +" é " + str(n1))
        if k2 == 0:
            primos.append(n2)
            a += 1
            print("O primo de numero " +  str(a) +" é " + str(n2))
            
            
        
         
            
        k1 = 0
        k2 = 0
        y = 0
        
        i += 1
        
    
    return primos
        
teste = primos(125000)
print("-----------------------------")
print(teste)
print("-----------------------------")
print(len(teste))
print("-----------------------------")
print("--- %s seconds ---" % (time.time() - start_time))

# sem nada ! 125000/500000 = 41538, 29.5 min
# tamanho/3 125000/500000 = 41539,  21.7 min   